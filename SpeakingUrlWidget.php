<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace robote13\speakingurl;

use yii\helpers\Html;

/**
 * Description of SlugWidget
 *
 * @author Tartharia
 */
class SpeakingUrlWidget extends \yii\widgets\InputWidget{
    
    public $toAttribute;
    
    /**
     * @var array the HTML attributes for the input tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $options = ['class' => 'form-control'];
    
    public function init() {
        parent::init();
        if(!isset($this->toAttribute))
        {
            throw new \yii\base\InvalidConfigException("'toAttribute' property must be specified.");
        }
    }
    
    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerClientScript();
        if ($this->hasModel()) {
            echo Html::activeInput('text', $this->model, $this->attribute, $this->options);
        } else {
            echo Html::input('text', $this->name, $this->value, $this->options);
        }
    }
    
    public function registerClientScript()
    {
        $view = $this->getView();        
        $view->registerAssetBundle(SpeakingUrlAsset::className());
        $sourceInputId = Html::getInputId($this->model, $this->toAttribute);
        $destInputId = Html::getInputId($this->model, $this->attribute);
        $view->registerJs(<<<JS
                var source = $('#{$sourceInputId}');
                var dest = $('#{$destInputId}');
                $('body').on('blur','#{$sourceInputId}',function(){
                    dest.val(getSlug(source.val()));
                });
JS
        ,  \yii\web\View::POS_READY,  $this->id);
    }
}
